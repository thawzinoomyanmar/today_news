import 'package:cloudsourcenews/views/homepage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:splashscreen/splashscreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  //This widget is the root of your application
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'CloudSource News',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: Colors.white,
        ),
        home: Scaffold(
            body: SafeArea(child: Builder(builder: (BuildContext context) {
          return  
            SplashScreen(
              seconds: 3,
      image: Image.asset(
        'assets/logo.jpg',
        alignment: Alignment(0, 0),
      ),
      photoSize: 150,
      backgroundColor: Colors.white,
      navigateAfterSeconds: HomePage(),
      loadingText: Text('loading. . .'),
            );
        }))));
  }
}

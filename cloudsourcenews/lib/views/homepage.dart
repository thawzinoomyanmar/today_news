import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloudsourcenews/helper/data.dart';
import 'package:cloudsourcenews/helper/widgets.dart';
import 'package:cloudsourcenews/models/categorie_model.dart';
import 'package:cloudsourcenews/views/categorie_news.dart';
import 'package:flutter/material.dart';
import 'package:flutter_offline/flutter_offline.dart';
import '../helper/news.dart';
import 'package:intl/intl.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool _loading;
  var newslist;

  List<CategorieModel> categories = List<CategorieModel>();
  ScrollController _scrollController = new ScrollController();
  void getNews() async {
    News news = News();
    await news.getNews();
    newslist = news.news;
    setState(() {
      _loading = false;
    });
  }
  //SQL LIte
  // void _jsonAndSqlite() async {
  //   const request =
  //       "https://newsapi.org/v2/top-headlines?sources=crypto-coins-news&apiKey=1bed424e0efb4755ba80353663787e3f";
  //   http.Response response = await http.get(request);
  //   debugPrint("Response: " + response.body);

  //   var myBigJSONObject = json.decode(response.body);
  //   var status = myBigJSONObject['status'];
  //   var totalResults = myBigJSONObject['totalResults'];
  //   var myArticles = myBigJSONObject['articles'];

  //   debugPrint("articles: " + myArticles.toString());

  //   var myFirstArticle = myArticles[0];
  //   var author = myFirstArticle['author'];
  //   var title = myFirstArticle['title'];

  //   // Get a location using getDatabasesPath
  //   var databasesPath = await getDatabasesPath();
  //   String path = join(databasesPath, 'test.db');

  //   // Delete the database
  //   await deleteDatabase(path);

  //   // open the database
  //   Database database = await openDatabase(path, version: 1,
  //       onCreate: (Database db, int version) async {
  //     // When creating the db, create the table
  //     await db.execute(
  //         'CREATE TABLE Article (id INTEGER PRIMARY KEY, author TEXT, title TEXT)');
  //   });

  //   // Insert some records in a transaction
  //   await database.transaction((txn) async {
  //     int id1 = await txn.rawInsert(
  //         'INSERT INTO Article(author, title) VALUES("$author", "$title")');
  //     debugPrint('inserted1: $id1');
  //   });
  // }

  //SQLite

  @override
  void initState() {
    // _jsonAndSqlite();
    _loading = true;
    super.initState();
    categories = getCategories();
    getNews();
    fetchFive();
    _scrollController.addListener(() {
      print(_scrollController.position.pixels);
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        //if we are the button of the page
        fetchFive();
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var strToday = getStrToday();
    return Scaffold(
        appBar: MyAppBar(),
        body: OfflineBuilder(
          connectivityBuilder: (BuildContext context,
              ConnectivityResult connectivity, Widget child) {
            final bool connected = connectivity != ConnectivityResult.none;
            return Stack(
              fit: StackFit.expand,
              children: [
                child,
                Positioned(
                  left: 0.0,
                  right: 0.0,
                  height: 32.0,
                  child: AnimatedContainer(
                    duration: const Duration(milliseconds: 300),
                    color: connected ? Color(0x00000000) : Color(0xFFEE4400),
                    child: connected
                        ? Text("")
                        : Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                " No Internet Connection",
                                style: TextStyle(color: Colors.white),
                              ),
                              SizedBox(
                                width: 8.0,
                              ),
                              SizedBox(
                                width: 12.0,
                                height: 12.0,
                                child: CircularProgressIndicator(
                                  strokeWidth: 2.0,
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Colors.white),
                                ),
                              ),
                            ],
                          ),
                  ),
                ),
              ],
            );
          },
          child: Center(
            child: SafeArea(
              child: _loading
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : SingleChildScrollView(
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            /// Categories
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 16),
                              height: 70,
                              child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  itemCount: categories.length,
                                  itemBuilder: (context, index) {
                                    return CategoryCard(
                                      imageAssetUrl:
                                          categories[index].imageAssetUrl,
                                      categoryName:
                                          categories[index].categorieName,
                                    );
                                  }),
                            ),
                            Divider(
                              color: Colors.black87,
                            ),
                            Padding(
                                padding: const EdgeInsets.all(19.0),
                                child: Column(children: <Widget>[
                                  Text("  Today News"),
                                  Text(
                                    strToday,
                                    style: (TextStyle(
                                      color: Color(0xFF325384).withOpacity(0.8),
                                      fontSize: 10.0,
                                    )),
                                  ),
                                ])),

                            /// News Article
                            Container(
                              margin: EdgeInsets.only(top: 16),
                              child: ListView.builder(
                                  controller: _scrollController,
                                  itemCount: newslist.length,
                                  shrinkWrap: true,
                                  physics: ClampingScrollPhysics(),
                                  itemBuilder: (context, index) {
                                    return NewsTile(
                                      imgUrl: newslist[index].urlToImage ?? "",
                                      title: newslist[index].title ?? "",
                                      desc: newslist[index].description ?? "",
                                      content: newslist[index].content ?? "",
                                      posturl: newslist[index].articleUrl ?? "",
                                    );
                                  }),
                            ),
                          ],
                        ),
                      ),
                    ),
            ),
          ),
        ));
  }

  String getStrToday() {
    var today = DateFormat().add_yMMMMd().format(DateTime.now());
    var strDay = today.split(" ")[1].replaceFirst(',', '');
    if (strDay == '1') {
      strDay = strDay + "st";
    } else if (strDay == '2') {
      strDay = strDay + "nd";
    } else if (strDay == '3') {
      strDay = strDay + "rd";
    } else {
      strDay = strDay + "th";
    }
    var strMonth = today.split(" ")[0];
    var strYear = today.split(" ")[2];
    return "$strDay $strMonth $strYear";
  }

  fetchFive() {
    for (int i = 0; i < 5; i++) {
      getNews();
    }
  }
}

class CategoryCard extends StatelessWidget {
  final String imageAssetUrl, categoryName;

  CategoryCard({this.imageAssetUrl, this.categoryName});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => CategoryNews(
                      newsCategory: categoryName.toLowerCase(),
                    )));
      },
      child: Container(
        margin: EdgeInsets.only(right: 14),
        child: Stack(
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(5),
              child: CachedNetworkImage(
                imageUrl: imageAssetUrl,
                height: 60,
                width: 120,
                fit: BoxFit.cover,
              ),
            ),
            Container(
              alignment: Alignment.center,
              height: 60,
              width: 120,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Colors.black26),
              child: Text(
                categoryName,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                    fontWeight: FontWeight.w500),
              ),
            )
          ],
        ),
      ),
    );
  }
}
